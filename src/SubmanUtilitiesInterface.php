<?php

namespace Drupal\subman;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Interface SubmanUtilitiesInterface.
 */
interface SubmanUtilitiesInterface {

  /**
   * Returns a config value for given key.
   *
   * @param string $key
   *   The key to which to return the settings value for.
   * @param mixed $default
   *   An optional default value in case no config could be retrieved.
   *
   * @return mixed
   *   The config value retrieved.
   */
  public function getSetting(string $key, $default = NULL):mixed;

  /**
   * Returns the subman logger.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   */
  public function logger(): LoggerChannelInterface;

  /**
   * Logs a message.
   *
   * @param string $message
   *   The message to log.
   */
  public function log(string $message, $data = NULL, array $context = [], string $level = 'info'): void;

  /**
   * Looks up an entity of given type using given key-value matches.
   *
   * @param string $entityType
   *   The entity type to look up.
   * @param $values
   *   The [key => value, ...] match(es) to look up.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity found.
   */
  public function retrieveEntityByField(string $entityType, array $values): ?ContentEntityInterface;

  /**
   * Dispatch event reusing event_dispatcher service.
   *
   * @param string $eventName
   *   The name of the event.
   * @param \Symfony\Contracts\EventDispatcher\Event $event
   *   The event object.
   */
  public function dispatchEvent(string $eventName, Event $event = NULL): void;

  /**
   * Gets the request time.
   *
   * @return int
   *   The request time.
   */
  public function getRequestTime(): int;

  /**
   * Explodes a string into an array of separated values.
   *
   * @param string $gluedInput
   *   The input string with the values still glued together.
   *
   * @return array
   *   The resulting array of separated and trimmed values.
   */
  public function explodeValues(string $gluedInput): array;

  /**
   * Gets the current user.
   *
   * @param \Drupal\user\UserInterface $user
   *   Any user to return instead of current user (helps with function that take an optional user parameter).
   *
   * @return \Drupal\user\UserInterface|null
   *   Returns the current user or NULL.
   */
  public function getCurrentUser(UserInterface $user = NULL): ?UserInterface;

  // ---------------------------------------------------------------------------
  // Data store.
  // ---------------------------------------------------------------------------

  /**
   * Stores given value under the given key.
   *
   * @param string $key
   *   The key to store the data with.
   * @param $value
   *   The data to store.
   * @param int $expirationTime
   *   The unix timestamp after which the data is considered invalid (permanent, if nothing specified).
   */
  public function saveData(string $key, $value, int $expirationTime = Cache::PERMANENT): void;

  /**
   * Retrieves the stored value for the given key.
   *
   * @param string $key
   *   The key to look for.
   *
   * @return mixed
   *   The cached data or NULL if not found.
   */
  public function loadCachedData(string $key): mixed;

}
