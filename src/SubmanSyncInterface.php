<?php

namespace Drupal\subman;

use Drupal\Core\Session\AccountInterface;
use Drupal\subman\Event\SubmanIncomingWebhook;
use Drupal\subman\Event\SubmanSubscriberUserModified;
use Drupal\subman\Event\SubmanSynchronousResult;
use Drupal\user\UserInterface;

/**
 * The Interface for the SubmanSync service.
 */
interface SubmanSyncInterface
{

  /**
   * Get the title of the subscription management service.
   *
   * @return string
   *   The title of the subscription management service.
   */
  public function getSubscriptionManagementServiceTitle(): string;

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents(): array;

  /**
   * Handle an incoming webhook event.
   *
   * @param \Drupal\subman\Event\SubmanIncomingWebhook $event
   *   The event with the data.
   */
  public function onIncomingWebhook(SubmanIncomingWebhook $event): void;

  /**
   * Handle a synchronous result event.
   *
   * @param \Drupal\subman\Event\SubmanSynchronousResult $event
   *   The event with the data.
   */
  public function onSynchronousResult(SubmanSynchronousResult $event): void;

  /**
   * Handle an incoming subscriber modified event.
   *
   * @param \Drupal\subman\Event\SubmanSubscriberUserModified $event
   *   The event with the subscriber data.
   */
  public function onSubscriberModified(SubmanSubscriberUserModified $event): void;

  /**
   * Gets the subscriber belonging to the given external primary key.
   *
   * In practice, a user is retrieved how carries the given id as its external id.
   *
   * @param $id_primary
   *   string The external primary id to look up against.
   *
   * @return ?\Drupal\user\UserInterface The subscriber entity found.
   */
  public function getUserBySaasId($saasId): ?UserInterface;

  /**
   * Deletes a user with the given data.
   *
   * @param string $saasId
   *   The external identifier of the user to be deleted.
   */
  public function deleteUser($saasSubscriberId, $cancelationMethod = 'user_cancel_delete'): void;

  /**
   * Attempts to sync data from the SaaS for and into the given subscriber.
   *
   * @param \Drupal\user\UserInterface $subscriber
   *   The user entity to sync with external system.
   * @param bool $forceFullLookup
   *   Whether to force a full subscriber data lookup instead of just using any
   *   external_id in stored sync data (which is the default).
   * @param bool $catchExceptions
   *  Whether to catch possible exceptions and instead write a log entry.
   *
   * @return bool
   *   Returns true, if sync was successful, false otherwise.
   */
  public function syncUser(UserInterface $subscriber, bool $forceFullLookup = FALSE, $catchExceptions = FALSE): bool;

  /**
   * Act on a login of a user.
   *
   * Try a first sync with subscription management service of user hasn't been synced yet.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function handleUserLogin(AccountInterface $account): void;

  /**
   * Returns an array of subscription data sub-arrays.
   *
   * If desired and by default, only valid subscriptions are returned.
   *
   * @param \Drupal\user\UserInterface $subscriber
   *   The content entiy / user that is the subscriber.
   * @param bool $limitToValid
   *   Whether or not to filter for only valid subscriptions (true by default).
   *
   * @return array
   *   The array of subscription data sub-arrays.
   */
  public function getUserSubscriptions(UserInterface $subscriber = NULL, bool $limitToValid = TRUE): array;

  /**
   * Gets the desired sync data from the given user object.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to get the sync data from.
   * @param string|null $key
   *   An optional sync data array key to return data for (by default, all sync
   *   data is returned).
   * @param bool $decodeJson
   *   Whether to decode the json.
   *
   * @return mixed
   *   The sync data as associative array.
   */
  public function getUserSaasData(UserInterface $user, string $key = NULL, bool $decodeJson = TRUE): mixed;

  /**
   * Determines the external ID of the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to look at.
   *
   * @return string|null
   *   The external ID or NULL, if none found.
   */
  public function getUserSaasId(UserInterface $user): ?string;

  /**
   * Determines whether given or current user is a synced subscriber.
   *
   * @param \Drupal\user\UserInterface $user
   *   Any optionally given user object.
   *
   * @return bool
   *   Whether given or current user is a synced subscriber.
   */
  public function isUserSubscriber(UserInterface $user = NULL): bool;

  /**
   * Retrieves the secondary id of the user.
   *
   * Extending classes could override this for e.g. customized checks.
   *
   * @param \Drupal\user\UserInterface $user
   *   The Drupal user to retrieve the secondary id from.
   *
   * @return string|null
   *   The secondary id, or NULL, if no valid secondary id could be determined.
   */
  public function getSecondaryId(UserInterface $user): ?string;

  /**
   * Gets and normalizes external subscriber by the given external id.
   *
   * @param string $saasSubscriberId
   *   The external id for a subscriber to retrieve the subscriber data by.
   *
   * @return array
   *   The array with the raw external data.
   */
  public function getSubscriberData(string $saasSubscriberId): array;

  /**
   * Get the complete subscriber data.
   *
   * Get and combine all the external data for a subscriber
   * (incl. subscriptions).
   *
   * @param string $saasSubscriberId
   *   The id of the customer to get updated data on.
   *
   * @return array
   *   The combined subscriber data in the structure:
   *     'subscriber' => [...]
   *     'subscriptions' => [[...], [...]]
   */
  public function getSubscriberDataComplete(string $saasSubscriberId): array;


  /**
   * Gets and normalizes external subscription by the given external id.
   *
   * @param string $saasSubscriberId
   *   The external id for a subscription to retrieve the subscription data by.
   *
   * @return array
   *   The array with the external data.
   */
  public function getSubscriptionData(string $saasSubscriberId): array;

  /**
   * Retrieves external subscriptions for the given subscriber external id.
   *
   * @param string $saasSubscriberId
   *   The external id for a subscriber to retrieve the subscriptions data for.
   *
   * @return array
   *   The array with the external data.
   */
  public function getSubscriberSubscriptionsData(string $saasSubscriberId): array;

  /**
   * Retrieves external subscriptions addons by the given external id.
   *
   * @param string $saasSubscriptionId
   *   The external id of the subscription to retrieve the addons for.
   *
   * @return array
   *   The array with the external data.
   */
  public function getSubscriptionAddonsData(string $saasSubscriptionId): array;

  /**
   * Get data of a specific subscription type.
   *
   * @param string $typeId
   *   Optional subscription type id to return the data for (by default, all
   *   types are returned as an array).
   *
   * @return array
   *   A subscription type data array for given subscription type,
   *   or array data items of all subscription types data, keyed by subscription
   *   type id.
   */
  public function getSubscriptionType(string $typeId): ?array;

  /**
   * Get data of all subscription types.
   *
   * @param bool $forceSaasRetrieval
   *   Indicates whether or not to force the data retrieval from saas.
   *
   * @return array
   *   Array of data items of all subscription types data, keyed by subscription
   *   type id.
   */
  public function getSubscriptionTypes(bool $forceSaasRetrieval = FALSE): array;


  /**
   * Builds a title for output of the given subscription data.
   *
   * @param array $subscriptionData
   *   The subscription data as structured array.
   *
   * @return string
   *   The assembled title for the subscription.
   */
  public function buildSubscriptionTitle(array $subscriptionData);

  /**
   * Builds and returns a render array element with an iframe to embed the $url.
   *
   * @param string $url
   *   The URL to show on the iframe embed.
   *
   * @return array
   *   Render array for displaying the iframe embed.
   */
  public function buildEmbed(string $url): array;

  /**
   * Builds and returns render array element to embed a signup form via iframe.
   *
   * @param string $embedParameter
   *   The URL parameters to extend to the URL.
   *
   * @return array
   *   The render array element to display the iframe embed.
   */
  public function buildEmbedSignup(string $embedParameter): array;

  /**
   * Build embedded self service.
   *
   * Builds and returns render array element to embed a self-service form via
   * iframe.
   * SECURITY IMPORTANT: The caller has to ensure that users may only access
   * self services they should be able to see! This API method doesn't offer
   * such checks.
   *
   * @param string $embedParameter
   *   The URL parameters to extend to the URL.
   *
   * @return array
   *   The render array element to display the iframe embed.
   */
  public function buildEmbedSelfservice(string $embedParameter): array;


}
