<?php

namespace Drupal\subman\Event;

use Drupal\subman\Exception\SubmanWebhookException;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a user logs in.
 */
class SubmanIncomingWebhook extends Event
{

  const EVENT_NAME = 'subman_incoming_webhook';

  /**
   * Constructs the object.
   *
   * @param string $service
   *   The passed service name from the webhook URL.
   * @param string $webhook
   *   The passed webhook name from the webhook URL.
   * @param string $dataRaw
   *   The passed raw string payload data.
   * @param string $data
   *   The decoded JSON data from the incoming webhook.
   */
  public function __construct(public string $service, public string $webhook, public string $dataRaw, public mixed $data = NULL)
  {
  }

  /**
   * Returns the SaaS event name, like "ContractCreated", ...
   *
   * @return string
   */
  public function getName(): string
  {
    return $this->getDataValue('Event');
  }

  /**
   * Returns the SaaS subscriber id.
   *
   * @return ?string
   */
  public function getSubscriberId(): ?string
  {
    return $this->getDataValue('CustomerId');
  }

  /**
   * Returns the SaaS subscription id.
   *
   * @return ?string
   */
  public function getSubscriptionId(): ?string
  {
    return $this->getDataValue('ContractId');
  }

  /**
   * Returns a data value from the event by its key.
   */
  protected function getDataValue($key): ?string
  {
    $json = $this->getData();
    SubmanWebhookException::throwOnEmpty($json, 'Empty or erroneous JSON data.');
    return $json[$key] ?? NULL;
  }

  /**
   * Get the value of service.
   *
   * @return string
   *   The service.
   */
  public function getService(): string
  {
    return $this->service;
  }

  /**
   * Get the value of webhook.
   *
   * @return string
   *   The webhook.
   */
  public function getWebhook(): string
  {
    return $this->webhook;
  }

  /**
   * Get the value of dataRaw.
   *
   * @return string
   *   The raw data.
   */
  public function getDataRaw(): string
  {
    return $this->dataRaw;
  }

  /**
   * Get the value of data.
   *
   * @return mixed
   *   The data.
   */
  public function getData(): mixed
  {
    return $this->data;
  }

}
