<?php

namespace Drupal\subman\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a user logs in.
 */
class SubmanSynchronousResult extends Event {

  const EVENT_NAME = 'subman_synchronous_result';

  /**
   * Constructs the object.
   *
   * @param string $differentiator
   *   Any URL differentiator that the URL might have specified.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request this event was triggered by.
   * @param string $redirectUrl
   *   The Url to redirect to after the event is handled.
   */
  public function __construct(public string $differentiator, public Request $request, public string $redirectUrl) {
  }

}
