<?php

namespace Drupal\subman\Event;

use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a subscriber Drupal user is modified
 */
class SubmanSubscriberUserModified extends Event {

  const EVENT_NAME = 'subman_subscriber_modified';

  const OP_PRE_CREATE = 'pre_create';
  const OP_PRE_UPDATE = 'pre_update';
  const OP_PRE_DELETE = 'pre_delete';

  const OP_POST_CREATE = 'post_create';
  const OP_POST_UPDATE = 'post_update';
  const OP_POST_DELETE = 'post_delete';

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $subscriber
   *   The affected entity modeling the subscriber.
   * @param string $operation
   *   The operation describing the change to the entity, see OP_* constants.
   */
  public function __construct(public UserInterface $subscriber, public string $operation = self::OP_PRE_UPDATE) {
  }

}
