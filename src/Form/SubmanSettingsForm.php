<?php declare(strict_types=1);

namespace Drupal\subman\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure SubMan Subscription Management Services settings for this site.
 */
final class SubmanSettingsForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'subman_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['subman.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('subman.settings');
    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#description' => $this->t('General subman settings'),
      '#description_display' => 'after',
    ];
    $form['settings']['fetch_overwrite_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fetch and override the subscriber Drupal user status from SaaS'),
      '#default_value' => $config->get('fetch_overwrite_status'),
      '#description' => $this->t('Fetched SaaS value should overwrite the user account status value'),
    ];
    $form['settings']['fetch_overwrite_mail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fetch and override the subscriber Drupal user mail from SaaS'),
      '#default_value' => $config->get('fetch_overwrite_mail'),
      '#description' => $this->t('Fetched SaaS value should overwrite the user account mail value'),
    ];
    $form['settings']['disable_drupal_mail_edit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable email address change in user profile'),
      '#default_value' => $config->get('disable_drupal_mail_edit'),
      '#description' => $this->t('SaaS should be the leading system for user data. To prevent different email addresses for the same account, you typically want to disable email address editing in user profiles.'),
      '#states' => [
        'visible' => [
          ':input[name="fetch_overwrite_mail"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['settings']['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging'),
      '#default_value' => $config->get('logging'),
      '#description' => $this->t('Write debugging log entries to the Drupal log.'),
    ];

    $form['notifications'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Notifications'),
      '#description' => $this->t('Subman notification templates'),
    ];
    $form['notifications']['trial_ending'] = [
      '#type' => 'details',
      '#title' => $this->t('Trial ending'),
      '#description' => $this->t('Subman notification templates'),
    ];
    $form['notifications']['trial_ending']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable trial ending notification'),
      '#default_value' => $config->get('notifications.trial_ending.enabled'),
    ];
    $form['notifications']['trial_ending']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('notifications.trial_ending.subject'),
    ];
    $form['notifications']['trial_ending']['body'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content'),
      '#default_value' => $config->get('notifications.trial_ending.body'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $this->config('subman.settings')
      ->set('fetch_overwrite_status', $form_state->getValue('fetch_overwrite_status'))
      ->set('fetch_overwrite_mail', $form_state->getValue('fetch_overwrite_mail'))
      ->set('disable_drupal_mail_edit', $form_state->getValue('disable_drupal_mail_edit'))
      ->set('logging', $form_state->getValue('logging'))
      ->set('notifications.trial_ending.subject', $form_state->getValue(['notifications', 'trial_ending', 'subject']))
      ->set('notifications.trial_ending.body', $form_state->getValue(['notifications', 'trial_ending', 'body']))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
