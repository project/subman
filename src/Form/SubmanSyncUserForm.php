<?php

namespace Drupal\subman\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\subman\SubmanSync;
use Drupal\subman\SubmanSyncInterface;
use Drupal\subman\SubmanUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SubmanSyncUserForm.
 */
class SubmanSyncUserForm extends FormBase {

  /**
   * The subman.utilities service.
   *
   * @var \Drupal\subman\SubmanUtilities
   */
  protected $submanUtils;

  /**
   * The subman.sync service.
   *
   * @var \Drupal\subman\SubmanSyncInterface
   */
  protected $submanSync;

  /**
   * Constructs a new SubmanSyncUserForm object.
   */
  public function __construct(SubmanUtilities $subman_utils, SubmanSyncInterface $subman_sync) {
    $this->submanUtils = $subman_utils;
    $this->submanSync = $subman_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('subman.utilities'),
      $container->get('subman.sync')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subman_sync_user_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AccountInterface $user = NULL) {
    if ($user) {
      // Remember user argument for later use in submit handler.
      $form_state->set('user', $user);

      // Explain what this is about.
      $form['info'] = [
        '#type' => 'container',
        'intro' => [
          '#prefix' => '<p><strong>',
          '#suffix' => '</strong></p>',
          '#markup' => $this->t('This action will <em>not</em> change any data at %saas. Only the Drupal values will be updated as follows:', [
            '%saas' => $this->submanSync->getSubscriptionManagementServiceTitle(),
          ]),
        ],
        'description' => [
          '#prefix' => '<p>',
          '#suffix' => '</p>',
          '#markup' => $this->t('By pressing the "Fetch"-button, %user\'s (UID %uid) data will be re-fetched from %saas, applying the latest state from :saas data that can be associated with this user (e.g. active subscription status and respective roles).', [
            '%uid' => $user->id(),
            '%user' => $user->getDisplayName(),
            '%saas' => $this->submanSync->getSubscriptionManagementServiceTitle(),
          ]),
        ],
      ];

      // Info about subscriber status.
      $form['info']['status'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('This user is <i>not</i> a subscriber, no external ID was found on the Drupal user.'),
      ];
      if ($this->submanSync->isUserSubscriber($user)) {
        $form['info']['status']['#markup'] = $this->t('This user is a subscriber with external ID @id.', [
          '@id' => $this->submanSync->getUserSaasId($user),
        ]);
      }

      // Info about last sync.
      $form['info']['last_sync'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('This user appears to be not synced yet.'),
      ];
      if ($last_sync = $this->submanSync->getUserSaasData($user, $this->submanSync::DATA_KEY_TIMESTAMP)) {
        $form['info']['last_sync']['#markup'] = $this->t('This user has been last synced on @last_sync_date.', [
          '@last_sync_date' => date('Y-m-d H:i:s', $last_sync),
        ]);

        // Sync details.
        $form['info']['details'] = [
          '#type' => 'details',
          '#title' => 'Detailed data',
          'data' => [
            '#prefix' => '<pre>',
            '#suffix' => '</pre>',
            '#markup' => $this->submanSync->getUserSaasData($user, NULL, FALSE),
          ],
        ];
      }

      $secondary_field_name = $this->submanUtils->getSetting('mapping.subscriber.secondary_id.drupal', SubmanSync::DRUPAL_FIELDNAME_ID_SECONDARY);
      $form['first_de_sync'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Desync & Resync: Force sync using user.%secondary_fieldname (%secondary_value) as criteria', [
          '%secondary_fieldname' => $secondary_field_name,
          '%secondary_value' => $this->submanSync->getSecondaryId($user),
        ]),
        '#description' => $this->t('Use this option to <strong>force a sync</strong>, as if the user has never beend synced before. This option <strong>empties the user fields for external id and sync data</strong>, forcing a user lookup at %saas using the secondary criteria: <em>user.@secondary_fieldname</em> instead of the the primary criteria in <em>user.@primary_fieldname</em>.', [
          '%saas' => $this->submanSync->getSubscriptionManagementServiceTitle(),
          '@primary_fieldname' => SubmanSync::DRUPAL_FIELDNAME_SAAS_ID,
          '@secondary_fieldname' => $secondary_field_name,
          '%secondary_value' => $this->submanSync->getSecondaryId($user),
        ]),
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Fetch "%user" data from %saas now', [
          '%user' => $user->getDisplayName(),
          '%saas' => $this->submanSync->getSubscriptionManagementServiceTitle(),
        ]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $form_state->get('user');

    if ($form_state->getValue('first_de_sync')) {
      $user->set('field_subman_sync', '');
      $user->set('field_subman_external_id', '');
      $user->save();
    }

    if ($this->submanSync->syncUser($user, TRUE, TRUE)) {
      $this->messenger()->addStatus($this->t('User "%user" has been synced with %saas.', [
        '%user' => $user->getDisplayName(),
        '%saas' => $this->submanSync->getSubscriptionManagementServiceTitle(),
      ]));
    }
  }

}
