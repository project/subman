<?php

namespace Drupal\subman\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\subman\SubmanSyncInterface;
use Drupal\subman\SubmanUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PreRegistrationForm.
 */
class PreRegistrationForm extends FormBase {

  /**
   * Egulias\EmailValidator\EmailValidator definition.
   *
   * @var \Egulias\EmailValidator\EmailValidator
   */
  protected $emailValidator;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The injected "renderer" service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Drupal\subman\SubmanSync definition.
   *
   * @var \Drupal\subman\SubmanSync
   */
  protected $submanSync;

  /**
   * Drupal\subman\SubmanUtilities definition.
   *
   * @var \Drupal\subman\SubmanUtilities
   */
  protected $utils;

  /**
   * Constructs a new PreRegistrationForm object.
   */
  public function __construct(
    EmailValidatorInterface $email_validator,
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer,
    SubmanSyncInterface $subman_sync,
    SubmanUtilities $utils
  ) {
    $this->emailValidator = $email_validator;
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->submanSync = $subman_sync;
    $this->utils = $utils;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('email.validator'),
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('subman.sync'),
      $container->get('subman.utilities')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subman_pre_registration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#cache']['max-age'] = 0;
    $form['#attached'] = [
      'library' => ['subman/iframe_embed'],
    ];

    // Placeholder for convenient placement of any custom steps indicator.
    $steps_indicator_text = 'Step :step_count';
    $form['steps_indicator_1'] = [
      '#type' => 'container',
      'step_output' => [
        '#markup' => $this->t($steps_indicator_text, [':step_count' => 1]),
      ],
    ];

    // Set second steps indicator for later.
    $steps_indicator_2 = $form['steps_indicator_1'];
    $steps_indicator_2['step_output']['#markup'] = $this->t($steps_indicator_text, [':step_count' => 2]);
    $form_state->set('steps_indicator_2', $steps_indicator_2);

    // Placeholder for ajax validation message.
    $form['validation_messages'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['validation-messages'],
      ],
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Your personal email address.'),
      '#weight' => '0',
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('email'),
    ];
    $form['email_confirmation'] = [
      '#type' => 'email',
      '#title' => $this->t('Confirm Email'),
      '#description' => $this->t('Re-enter your personal email address to ensure correct typing.'),
      '#weight' => '1',
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('email_confirmation'),
    ];
    $form['continue'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
      '#weight' => '10',
      '#ajax' => [
        'callback' => '::continueViaAjax',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('email') !== $form_state->getValue('email_confirmation')) {
      $form_state->setErrorByName('email_confirmation', t('The email addresses you enter into the email confirmation field must be the same as the one you enter into the email field.'));
    }

    if ($this->utils->retrieveEntityByField('user', ['mail' => $form_state->getValue('email')])) {
      $form_state->setErrorByName('email', t('An account with this email address already exists.'));
    }
  }

  /**
   * {@inheritdoc}
   *
   * Empty, since we handle everything via ajax.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Provides an Ajax response add display validation error messages via ajax.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AjaxResponse with the added validation error messages.
   */
  public function addAjaxValidationMessages(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $errors = $form_state->getErrors();
    // Get rid of any drupal system messages which might display the same
    // validation error messages on the next page load.
    $this->messenger()->deleteAll();

    $messages = [];
    foreach ($errors as $element_name => $error) {
      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $error */
      $messages['error'][] = $this->t($error->render());
    }

    $message_build = [
      '#theme' => 'status_messages',
      '#message_list' => $messages,
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
      ],
    ];
    $message_build = $this->renderer->render($message_build);

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('.validation-messages', $message_build));
    return $response;
  }

  /**
   * Handles ajax submission to validate form and continue to embedded signup.
   */
  public function continueViaAjax(array $form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return $this->addAjaxValidationMessages($form, $form_state);
    }
    else {
      // Execute any (additional) submit handlers on the 'continue' button:
      // Get subscription type id from the first form build argument.
      $id = reset($form_state->getBuildInfo()['args']);

      // Placeholder for convenient placement of any custom steps indicator.
      $steps_indicator_2 = $form_state->get('steps_indicator_2');
      $embed = ['steps_indicator_2' => $steps_indicator_2];

      // Build and render signup embed.
      $embed['embed'] = $this->submanSync->buildEmbedSignup($id, trim($form_state->getValue('email')));

      // Render the render array.
      $embed = $this->renderer->render($embed);

      // Add html code for signup embed to form via ajax response.
      $response = new AjaxResponse();
      $response->addCommand(new HtmlCommand('.subman-pre-registration-form', $embed));
      return $response;
    }
  }

}
