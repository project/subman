<?php

declare(strict_types = 1);

namespace Drupal\subman;

/**
 * Helper service for Environments handling.
 */
final class SubmanEnvironments {

  const ENVIRONMENT_SANDBOX = 'sandbox';
  const ENVIRONMENT_PRODUCTION = 'production';

  const SETTINGS_KEY_CREDENTIALS = 'credentials';
  const SETTINGS_KEY_ENVIRONMENT = 'environment';
  const SETTINGS_KEY_CLIENT_ID = 'client_id';
  const SETTINGS_KEY_CLIENT_SECRET = 'client_secret';

  /**
   * Constructs an Environments object.
   */
  public function __construct(
    private readonly SubmanUtilitiesInterface $submanUtilities,
  ) {}

  /**
   * Returns the current Billwerk environment to use.
   *
   * One of:
   * - SubmanEnvironments::ENVIRONMENT_SANDBOX
   * - SubmanEnvironments::ENVIRONMENT_PRODUCTION
   * typically defined in the settings.php.
   *
   * @return string
   *   The current environment.
   */
  public function getCurrentEnvironment(): string {
    return $this->submanUtilities->getSetting(self::SETTINGS_KEY_ENVIRONMENT, self::ENVIRONMENT_SANDBOX);
  }

  /**
   * Returns the current Billwerk Client ID.
   *
   * @return string
   *   The client ID.
   */
  public function getClientId():string {
    return $this->submanUtilities->getSetting(self::SETTINGS_KEY_CLIENT_ID);
  }

  /**
   * Returns the current Billwerk Client secret.
   *
   * @return string
   *   The client secret.
   */
  public function getClientSecret(): string {
    return $this->submanUtilities->getSetting(self::SETTINGS_KEY_CLIENT_SECRET);
  }

}
