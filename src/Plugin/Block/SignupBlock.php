<?php

namespace Drupal\subman\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\subman\SubmanEmbedsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SignupBlock' block.
 *
 * @Block(
 *  id = "subman.signup_block",
 *  admin_label = @Translation("Signup block"),
 * )
 */
class SignupBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Subman embeds helper service.
   *
   * @var \Drupal\subman\SubmanEmbedsHelper
   */
  protected $submanEmbedsHelper;

  /**
   * Constructs a new SignupBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SubmanEmbedsHelper $submanEmbedsHelper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->submanEmbedsHelper = $submanEmbedsHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('subman.embeds_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'subscription_type_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['subscription_type_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subscription type ID'),
      '#description' => $this->t('The ID of the subscription (sub-)type to register for. (For Billwerk this is called Plan Variant).'),
      '#default_value' => $this->configuration['subscription_type_id'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['subscription_type_id'] = $form_state->getValue('subscription_type_id');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $subscriptionTypeId = $this->configuration['subscription_type_id'];
    return $this->submanEmbedsHelper->buildSignupEmbed($subscriptionTypeId);
  }

}
