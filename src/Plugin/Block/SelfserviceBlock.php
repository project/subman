<?php

namespace Drupal\subman\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\subman\SubmanEmbedsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'SelfserviceBlock' block.
 *
 * @Block(
 *  id = "subman.selfservice_block",
 *  admin_label = @Translation("Selfservice block"),
 * )
 */
class SelfserviceBlock extends BlockBase implements ContainerFactoryPluginInterface {

  const QUERYSTRING_ID_KEY = 'id';

  /**
   * Constructs a new SelfserviceBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\subman\SubmanSync $subman_sync
   *   The injected subman.sync service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The injected request_stack service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected RequestStack $requestStack,
    protected SubmanEmbedsHelper $submanEmbedsHelper,
    protected AccountProxyInterface $currentUser,
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('subman.embeds_helper'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user_id = $this->currentUser->id();
    $user = $this->entityTypeManager->getStorage('user')->load($user_id);

    // @todo Is it really a good idea to use the URL id parameter? (historic code)
    $subscriptionId = $this->requestStack->getCurrentRequest()->get(self::QUERYSTRING_ID_KEY);

    if (!empty($subscriptionId)) {
      return $this->submanEmbedsHelper->buildSelfserviceEmbedForSubscription($user, $subscriptionId);
    }
    else {
      return $this->submanEmbedsHelper->buildSelfserviceEmbedForSubscriptions($user);
    }
  }

}
