<?php

namespace Drupal\subman\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Action to sync a given user with an external subscription management service,
 * managed by the subman.module, as core action variant.
 *
 * @Action(
 *   id = "subman_user_sync_action",
 *   label = @Translation("Sync user with subscription management service"),
 *   type = "user",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "subman_manual_user_sync",
 *   },
 * )
 */
class SubmanUserSyncAction extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /** @var \Drupal\user\UserInterface $entity */
    if ($entity->getEntityTypeId() == 'user') {
      // Do NOT use dependency injection here, as this service is abstract and
      // needs to be decorated by a child module, (the dependency injection for
      // this class is executed on module installation) see the billwerk module:
      \Drupal::service('subman.sync')->syncUser($entity, TRUE, TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $account->hasPermission('subman.permissions.yml');
  }

}
