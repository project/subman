<?php

namespace Drupal\subman\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\subman\SubmanSyncInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Action to notify a given user about an approaching end of their trial subscription,
 * managed by the subman.module, as core action variant.
 *
 * @Action(
 *   id = "subman_notify_trial_end_approaching",
 *   label = @Translation("Notify user about the trial soon ending."),
 *   type = "user",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "subman_manual_user_sync",
 *   },
 * )
 */
class SubmanNotifyTrialEndApproaching extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\subman\SubmanSyncInterface definition.
   *
   * @var \Drupal\subman\SubmanSyncInterface
   */
  protected $submanSync;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\subman\SubmanSyncInterface $subman_sync
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SubmanSyncInterface $subman_sync) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->submanSync = $subman_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('subman.sync')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /** @var \Drupal\user\UserInterface $entity */
    if ($entity && $entity->getEntityTypeId() == 'user') {
      $this->submanSync->notifyUserOfTrialEnd($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // @todo is this correct (the permissions yaml file name instead of a permission machine name?
    return $account->hasPermission('subman.permissions.yml');
  }

}
