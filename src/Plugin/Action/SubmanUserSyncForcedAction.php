<?php

namespace Drupal\subman\Plugin\Action;

/**
 * Action to sync a given user with an external subscription management service
 * (forcing to desync and this using the secondary criteria),
 * managed by the subman.module, as core action variant.
 *
 * @Action(
 *   id = "subman_user_sync_forced_action",
 *   label = @Translation("Sync user with subscription management service (forced)"),
 *   type = "user",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "subman_manual_user_sync",
 *   },
 * )
 */
class SubmanUserSyncForcedAction extends SubmanUserSyncAction {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // First de-sync.
    if ($entity->getEntityTypeId() == 'user') {
      $entity->set('field_subman_sync', '');
      $entity->set('field_subman_external_id', '');
      $entity->save();
    }

    // Then re-sync.
    parent::execute($entity);
  }

}
