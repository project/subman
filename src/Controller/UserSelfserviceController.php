<?php

declare(strict_types = 1);

namespace Drupal\subman\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\subman\SubmanEmbedsHelper;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for subman Integration routes.
 */
final class UserSelfserviceController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('subman.embeds_helper')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\subman\SubmanEmbedsHelper $submanEmbedsHelper
   */
  public function __construct(private SubmanEmbedsHelper $submanEmbedsHelper) {
  }

  /**
   * Undocumented function.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return array
   */
  public function buildManageSubscriptions(UserInterface $user): array {
    return $this->submanEmbedsHelper->buildSelfserviceEmbedForSubscriptions($user);
  }

  /**
   * Undocumented function.
   *
   * @param \Drupal\user\UserInterface $user
   * @param string $subscriptionId
   *
   * @return array
   */
  public function buildManageSubscription(UserInterface $user, string $subscriptionId): array {
    return $this->submanEmbedsHelper->buildSelfserviceEmbedForSubscription($user, $subscriptionId);
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    if ($account->id() === \Drupal::currentUser()->id()) {
      // Is the current users account.
      return AccessResult::allowedIfHasPermission($account, 'subman_access_own_manage_subscriptions_user_tab');
    }
    else {
      // Is a different users account.
      return AccessResult::allowedIfHasPermission($account, 'subman_access_any_manage_subscriptions_user_tab');
    }
  }

}
