<?php

namespace Drupal\subman\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\subman\Event\SubmanSynchronousResult;
use Drupal\subman\SubmanUtilities;
use Drupal\subman\Exception\SubmanWebhookException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class SubmanWebhook.
 */
class SubmanSynchronousResultController extends ControllerBase
{

  /**
   * The event_dispatcher service.
   *
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Drupal\subman\SubmanUtilities definition.
   *
   * @var \Drupal\subman\SubmanUtilities
   */
  protected $utils;

  /**
   * Constructs a new SubmanWebhook object.
   *
   * @param \Drupal\subman\SubmanUtilities $subman_utilities
   *   The subman utilities service.
   * @param EventDispatcherInterface $event_dispatcher
   *   The injected event_dispatcher service.
   */
  public function __construct(SubmanUtilities $subman_utilities, EventDispatcherInterface $event_dispatcher)
  {
    $this->eventDispatcher = $event_dispatcher;
    $this->utils = $subman_utilities;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('subman.utilities'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Receives an incoming webhook and passes its payload to the sync service.
   *
   * @param string $differentiator
   *   Any string to differentiate the url.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object which triggered the controller.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The output as render array.
   */
  public function receive(string $differentiator, Request $request): array|RedirectResponse
  {
    $status = 'ok';
    $message = $this->t('Thank you!');
    $redirect_url = $this->utils->getSetting('synchronous_result_redirect_url');

    // Log receiving of webhook.
    $this->utils->log('receive(): Synchronous result received: @differentiator @request_url', NULL, [
      '@differentiator' => $differentiator,
      '@request_url' => $request->getUri() . '?' . $request->getQueryString(),
    ], 'debug');

    // Trigger SubmanIncomingWebhook event to let specific integration modules
    // act upon the webhook.
    try {
      $event = new SubmanSynchronousResult($differentiator, $request, $redirect_url);
      $this->utils->dispatchEvent(SubmanSynchronousResult::EVENT_NAME, $event);
      $redirect_url = $event->redirectUrl;
    } catch (SubmanWebhookException $exception) {
      $status = $exception->getCode();
      $message = $this->t('Error occured during processing: @message', ['@message' => $exception->getMessage()]);
    }

    // Log receiving of webhook.
    $this->utils->log('receive(): Synchronous result received: @differentiator @request_url: @status/"@message".', NULL, [
      '@differentiator' => $differentiator,
      '@request_url' => $request->getUri() . '?' . $request->getQueryString(),
      '@status' => $status,
      '@message' => $message,
    ], $status != 'ok' ? 'debug' : 'error');

    // Output message if status not ok or no redirect url is configured.
    if (($status != 'ok') || empty($redirect_url)) {
      return ['#markup' => $message];
    } else {
      return new RedirectResponse($redirect_url);
    }
  }

}
