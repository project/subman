<?php

namespace Drupal\subman\Exception;

use Drupal\Component\Serialization\Json;

class SubmanException extends \Exception
{

  public string $additionalInformation;

  public function __construct($message = "", $code = 0, $previous = NULL, mixed $additionalInformation = NULL)
  {
    if (!is_string($additionalInformation)) {
      // @todo:
      dsm(Json::encode($additionalInformation));
      $this->additionalInformation = Json::encode($additionalInformation);
    }
    parent::__construct($message, $code, $previous);
  }
}
