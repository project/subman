<?php

namespace Drupal\subman\Exception;

/**
 * Class defining subman webhook exceptions.
 */
abstract class SubmanWebhookException extends SubmanException
{

  /**
   * The HTTP status code of this exception.
   */
  protected ?int $httpStatusCode = NULL;

  protected ?string $messagePrefix = '';

  /**
   * {@inheritDoc}
   */
  public function __construct($message = "", $code = 0, $previous = NULL, mixed $additionalInformation = NULL)
  {
    parent::__construct($this->messagePrefix . $message, $code ?: $this->httpStatusCode, $previous, $additionalInformation);
  }

  /**
   * Throws an exception if the data at the key is empty.
   */
  public static function throwOnEmpty($data, $key = NULL)
  {
    if (empty($data)) {
      throw new SubmanWebhookDataMissingException($key);
    }
  }

}
