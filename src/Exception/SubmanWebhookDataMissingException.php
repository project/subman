<?php

namespace Drupal\subman\Exception;

/**
 *
 */
class SubmanWebhookDataMissingException extends SubmanWebhookException {

  protected ?int $httpStatusCode = 500;

  protected ?string $messagePrefix = 'Data missing: ';
}
