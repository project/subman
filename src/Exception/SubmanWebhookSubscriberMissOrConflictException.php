<?php

namespace Drupal\subman\Exception;

/**
 *
 */
class SubmanWebhookSubscriberMissOrConflictException extends SubmanWebhookException {
  protected ?int $httpStatusCode = 500;

  protected ?string $messagePrefix = 'Subscriber missed or conflict:';
}
