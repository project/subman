<?php

namespace Drupal\subman\Exception;

/**
 *
 */
class SubmanApiErrorException extends SubmanWebhookException {
  protected ?int $httpStatusCode = 422;

  protected ?string $messagePrefix = 'Error returned from API';
}
