<?php

namespace Drupal\subman;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class SubmanUtilities.
 */
class SubmanUtilities implements SubmanUtilitiesInterface
{

  const STORE_PREFIX = 'subman_store:';

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $submanConfig;

  /**
   * Constructs a new SubmanUtilities object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   EntityTypeManager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   ConfigFactory.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   ContainerAwareEventDispatcher.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   LoggerChannelFactoryInterface.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   AccountProxy.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
    protected readonly ContainerAwareEventDispatcher $eventDispatcher,
    protected readonly LoggerChannelFactoryInterface $loggerFactory,
    protected readonly AccountProxyInterface $currentUser
  ) {
    $this->submanConfig = $configFactory->get('subman.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function getSetting(string $key, $default = NULL): mixed
  {
    $value = $default;
    $settings = Settings::get('subman.settings');
    if (is_array($settings) && isset($settings[$key])) {
      $value = $settings[$key];
    }
    return $value;
  }


  /**
   * {@inheritDoc}
   */
  public function logger(): LoggerChannelInterface
  {
    return $this->loggerFactory->get('subman');
  }

  /**
   * {@inheritDoc}
   */
  public function log(string $message, $data = NULL, array $context = [], string $level = 'info'): void
  {
    if (!$this->submanConfig->get('logging')) {
      // Do not log, if logging is disabled.
      return;
    }
    $sep = "<br />\n";

    // Get logger.
    $logger = $this->loggerFactory->get('subman');
    $method = method_exists($logger, $level) ? $level : 'debug';

    // Ammend caller info.
    $dbt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
    $caller = isset($dbt[1]) ? $sep . 'Caller: ' . implode(':', $dbt[1]) : '';
    $message .= $caller;

    // Add data to context and message.
    if ($data) {
      $data = is_string($data) ? $data : json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS);
      $context['@subman_log_data'] = $data;
      $message .= $sep . 'Data:' . $sep . '<pre>@subman_log_data</pre>';
    }

    // Log message.
    $logger->{$method}($message, $context);
  }

  /**
   * {@inheritDoc}
   */
  public function retrieveEntityByField(string $entityType, array $values): ?ContentEntityInterface
  {
    $storage = $this->entityTypeManager->getStorage($entityType);
    $query = $storage->getQuery();
    foreach ($values as $key => $value) {
      if (is_null($value)) {
        $query->condition($key, NULL, 'IS NULL');
      } else {
        $query->condition($key, $value);
      }
    }
    $query->range(0, 1);
    $ids = $query
      ->accessCheck(TRUE)
      ->execute();

    $entities = $storage->loadMultiple($ids);

    return empty($entities) ? NULL : reset($entities);
  }

  /**
   * {@inheritDoc}
   */
  public function dispatchEvent(string $eventName, Event $event = NULL): void
  {
    $this->eventDispatcher->dispatch($event, $eventName);
  }

  /**
   * {@inheritDoc}
   */
  public function getRequestTime(): int
  {
    // @todo properly inject time service.
    return \Drupal::time()->getRequestTime();
  }

  /**
   * {@inheritDoc}
   */
  public function explodeValues(string $gluedInput): array
  {
    $separator = $this->getSetting('separator', ',');
    $values = explode($separator, $gluedInput);
    foreach ($values as $key => $value) {
      $values[$key] = trim($value);
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function getCurrentUser(UserInterface $user = NULL): ?UserInterface
  {
    if (empty($user)) {
      $user_id = $this->currentUser->id();
      $user = $this->entityTypeManager->getStorage('user')->load($user_id);
    }
    return $user;
  }

  // ---------------------------------------------------------------------------
  // Data store.
  // ---------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   */
  public function saveData(string $key, $value, int $expirationTime = Cache::PERMANENT): void
  {
    \Drupal::cache()->set(self::STORE_PREFIX . $key, $value, $expirationTime);
  }

  /**
   * {@inheritDoc}
   */
  public function loadCachedData(string $key): mixed
  {
    $cache = \Drupal::cache()->get(self::STORE_PREFIX . $key);
    return $cache ? $cache->data : NULL;
  }

}
