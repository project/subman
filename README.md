# Developer Documentation - subman and billwerk modules

Daniel Nolde – nolde.consulting@gmail.com

This document should give you as developer an idea how to use and extend this module (family) and to be able to extend it in a clean way.

# Purpose of modules

The purpose of the subman/billwerk modules is to connect Drupal 8+ to a subscription management SaaS (in the following just called "SaaS"), like Billwerk.
The idea is that a subscription based business uses the best tools for each technical domain:

- An external SaaS as business "backend" to handle and automate the core business aspects of subscription management and its processes (like ordering, billing or payment) and entities (like subscribers, their subscriptions or invoices etc).
- Drupal as business "frontend" to handle content, roles and permissions.
- The subman/billwerk modules to make and keep the connection between the two: Via that interface, a Drupal user gets their up-to-date personal data and roles assigned (and thus permissions), from the subscriber and subscription data in the subscription management SaaS, everytime something happens to that data.

This way, the business can use specialized solutions for both its ends. As a result, its Web (frontend) application with Drupal will stay leaner and cleaner, and the backend processes will be more secure and stable.

# Subscription management SaaSs (e.g. Billwerk)

...
(t.b.d. - some explanations of what a SaaS like Billwerk does and how it maps data internally. Necessary?)
...

# Scope

## What's covered by this module

- Syncing of a Drupal user to the according SaaS subscriber
  - initially on first login
  - on change of data or state of a subscriber or its subscription(s)
  - manually by a site manager having the necessary permission
  - This covers:
    - personal data (such as email) managed in the SaaS
    - subscription information
    - roles depending on types of a user's subscription
- Including forms of the SaaS into Drupal, for ...
  - ordering subscriptions and becoming a subscriber
  - changing personal or payment data
- Notifying users, of ...
  - their new account created during subscription
  - the approaching end of a trial period

General idea: Reactive and uni-directional transfer of data from the SaaS to Drupal. I.e.: whatever happens to the data managed by SaaS is happening and triggered via the SaaS, not Drupal. Drupal is only at the receiving end of data or state changes.

## What's _not_ covered

- NO bi-directional data transfer from Drupal to the SaaS
  (i.e.: no changing of SaaS data triggered from/by Drupal)
- NO modeling of SaaS data entities as Drupal entities
- NO _custom_ forms for ordering/subbscribing or self-service changes

# Architecture

## Code Organisation

The functionality provided (connecting to a SaaS, receiving data from it, finding and updating corresponding users) is distributed across classes in several modules - SaaS-independent code, SaaS specific code and project specific code/customizations.

The reason for this is to get a clean separation of Drupal specific logic, SaaS specific logic, and business/project specific logic, and to become a better base for a more extensible module that can be contributed to the community.

The "subman" base module provides a foundation for handling those aspects that can be coded independently of a specific SaaS.

An additional dependent module like "billwerk" provides SaaS specific functionality via additional classes and service.

Project specific logic is provided by overriding services with customized derived classes in a project specific module.

## Class relationships

![subman architecture](./docs/subman-architecture.svg 'Architecture of subman modules and classes')

SVG: [subman architecture](./docs/subman-architecture.svg)

Source: https://docs.google.com/drawings/d/1_DAoVqqgiLk_d7e2i5XpGvt10H3dSfgTm217Y36URHg

## Principles

The interface functionality is focused to be:

- Reactive
- Unidirectional
- Only one SaaS integration per Drupal instance at a time
- Customizable

## Lingo

| Subman                  | Drupal | Billwerk                 |
| ----------------------- | ------ | ------------------------ |
| suscriber               | user   | customer                 |
| subscription            |        | contract                 |
| subscription (sub-)type |        | plan(-variant)           |
| add-on                  |        | component(-subscription) |

Term *Subscriber* is used for the remote entity, while *User* is used for the local one.

The specific SaaS may offer **hosted pages** by which an end user can either _signup_ or _order a subscription_ or perform a **self-service** to change personal or payment data.

A **webhook** that is provided by subman as a Drupal controller (url see "Installation") is triggered by the SaaS to notify Drupal via a **webhook event** of any change of data or state.

## Services

sync (using/extending one abstract base class)
utils
api (SaaS module specific, no abstract base class yet)

## Modules

## Classes

# Actions

- Manual sync
  ...
- Bulk sync
  ...

# Events

- SubmanIncomingWebhook
  Triggered by SaaS to notify Drupal of any change in data via a webhook event.
- SubmanSubscriberModified
  Triggered by SubmanSync to notify any interested module that a Drupal user has
  been updated via the integration.
- SubmanSynchronousResult
  Triggered by SubmanSynchronousResultController in response to a successful
  signup completed by an end user via the SaaS's signup widget/iframe/page.

# Customization

- Project specific customizations by overriding "suman.sync" service with a
  custom class (e.g. CustomBillwerkSync) which extends the service class provided
  by the module for your specific SaaS (e.g. the BillwerkSync class provided by
  the billwerk.module).

# Debugging

For tipps for debugging:

- Look into your Drupal watchdog messages of module "subman" – most errors will
  be logged there, as well as incoming webhook events or manually triggered sync events.

- Use an application like Insomnia to simulate webhook calls a they would be
  triggered by the SaaS.

- Use devel.module's php execution, drush eval or drush php to trigger relevant
  actions, like:

  ```
  $sync = \Drupal::service("subman.sync");
  $api = \Drupal::service("billwerk.api");
  $user = \Drupal\user\Entity\User::load(12345);

  // Sync a specific user:
  $sync->syncSubscriber($user);

  // Get the SaaS data the integrations finds for given user:
  $sync->fetchUserSaasData($user);

  // Get all subscription types/subtypes from Billwerk:
  $api->getPlans();

  // Get SaaS data for a specific subscription type by its external SaaS id:
  $sync->getSubscriptionType("ab12cd45...");

  // Get the subscriptions of a subscriber by its external SaaS id:
  $sync->retrieveSubscriptionsForExternalSubscriberId("1234abcd");

  ```

# Extending

...

## APIs of SaaS

Billwerk API documentation: https://developer.billwerk.io

# ToDos

- Update interfaces for classes (prerequisite for contribution as well as unit
  testing)
- Refactoring and improvements as stated in the @Todo code comments

# Shortcomings

...

# Installation

- Enable subman module and a dependent integration module for a specific
  subscription management SaaS (e.g. billwerk).
- Ensure the field user.field_subman_external_id exists as type text (plain).
- Ensure the field user.field_subman_sync exists as type JSON (json_native).
  - Note, that this type comes from the dependant "json_field" module.
- Configure the following webhook to your SaaS for the events supported by you
  specific integration module that are relevant to you use case:
  https://publicly.accessible.domain/subscription-management-service/receive/SAAS_NAME/general
  (SAAS_NAME can be set to your SaaS's name, e.g. "billwerk" or "saas-01", and be
  used for some arbitrary distinction).
- Configure
  https://publicly.accessible.domain/subscription-management-service/synchronous-result/DIFFERENTIATOR
  (Where DIFFERENTIATOR is just anything you want) as a signup success url in your
  SaaS if it supports that. This Url is called then by your SaaS after it's signup
  widget/iframe/page has been used by an end user for a successful signup (via
  ordering a subscription product). This Url's controller will then try a first
  sync on that subscriber, create a Drupal user for it, and redirect to a further
  content url, if you specify one in your configuration setting
  'synchronous_result_redirect_url' (see section "Configuration").
- To provide end user facing facilities for signup and self-service changes,
  create a page or panel containing the following blocks:
  - SelfserviceBlock – outputs the SaaS embed that allow a user to change
    personal and payment data
  - SignupBlock – outputs the SaaS embed that allows a visitor to signup
- Add to settings.php the api keys as well as any other of the configuration
  settings of the following section that you find necessary.

# Configuration

Configuration settings can be specified in your settings.php, for example (for
more, look into the module's code whenever the method config() of class
SubmanUtilities is used):

```php
// Specify the which instance of the SaaS to connect to with what api keys
// (as environment specific settings; you'd have to determine your environment):
if ($env_is_live_server) {
  // Connect your production web environment to your SaaS's production instance:
  // The environment - must be "production" or "sandbox"
  // See https://www.drupal.org/project/subman/issues/3403600
  $settings['subman.settings'][SubmanEnvironments::SETTINGS_KEY_ENVIRONMENT] = SubmanEnvironments::ENVIRONMENT_PRODUCTION;
  // You can find the client ID & secret on your provider API settings page:
  $settings['subman.settings'][SubmanEnvironments::SETTINGS_KEY_CREDENTIALS][SubmanEnvironments::ENVIRONMENT_PRODUCTION][SubmanEnvironments::SETTINGS_KEY_CLIENT_ID] = 'YOUR_SAAS_PRODUCTION_CLIENT_ID';
  $settings['subman.settings'][SubmanEnvironments::SETTINGS_KEY_CREDENTIALS][SubmanEnvironments::ENVIRONMENT_PRODUCTION][SubmanEnvironments::SETTINGS_KEY_CLIENT_SECRET] = 'YOUR_SAAS_PRODUCTION_CLIENT_SECRET';
}
else {
  // Connect your other web environments to your SaaS's sandbox instance:
  // The environment - must be "production" or "sandbox"
  // See https://www.drupal.org/project/subman/issues/3403600
  $settings['subman.settings'][SubmanEnvironments::SETTINGS_KEY_ENVIRONMENT] = SubmanEnvironments::ENVIRONMENT_SANDBOX;
  // You can find the client ID & secret on your provider API settings page:
  $settings['subman.settings'][SubmanEnvironments::SETTINGS_KEY_CREDENTIALS][SubmanEnvironments::ENVIRONMENT_SANDBOX][SubmanEnvironments::SETTINGS_KEY_CLIENT_ID] = 'YOUR_SAAS_SANDBOX_CLIENT_ID';
  $settings['subman.settings'][SubmanEnvironments::SETTINGS_KEY_CREDENTIALS][SubmanEnvironments::ENVIRONMENT_SANDBOX][SubmanEnvironments::SETTINGS_KEY_CLIENT_SECRET] = 'YOUR_SAAS_SANDBOX_CLIENT_SECRET';
}

// Map any additional external data properties to user fields:
$settings['subman.settings']['mapping.subscriber'] = [
  'field_firstname' => 'FirstName',
  'field_lastname'  => 'LastName',
];

// Use alternative secondary identifier in case a user or SaaS subscriber has to
// be determined without a external id as primary identifier:
$settings['subman.settings']['mapping.subscriber.secondary_id.drupal'] = 'field_customer_number'; // default: user.mail
$settings['subman.settings']['mapping.subscriber.secondary_id.saas'] = 'DebitorAccount';

// Declare which roles should be managed and applicable via the SaaS
// integration:
$settings['subman.settings']['roles_applicable'] = [
  'subscriber',
  'subscriber_trial',
  'subscriber_edu',
  'license_company',
  'license_educational',
];

// Set any custom url the integration's signup success endpoint should redirect
// to (e.g. a page node's url with further explanations for the user):
$settings['subman.settings']['synchronous_result_redirect_url'] = '/signup/thanks';

```
